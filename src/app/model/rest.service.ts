import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from './product.model';
import { Category } from './category.model';
@Injectable()
export class RestService {
  basuUrl : string = 'myBackendURl';
  constructor(private http: HttpClient) { }
  getProducts(): Observable<Product[]>{
    return this.http.get<Product[]>(this.basuUrl+'products')
  }
  getCategories():Observable<Category[]>{
    return this.http.get<Category[]>(this.basuUrl+'category')
  }
}
