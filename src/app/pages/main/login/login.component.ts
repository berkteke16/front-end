import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { LoginServiceService } from '../../services/login-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  constructor(
    private loginServiceService: LoginServiceService,
    private readonly fb: FormBuilder) {

    this.form = this.fb.group({
      username: [''],      
      password: ['']
    });
  }

  submitForm() {

    console.log(this.form.value);

    const user = this.form.value;
    console.log(user);

    this.loginServiceService.createTask(user);
  }

  ngOnInit(): void {

  }

}
